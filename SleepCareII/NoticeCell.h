//
//  NoticeCell.h
//  SleepCareII
//
//  Created by mengqinghao on 14-6-19.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol deleteTixingDelegate <NSObject>

-(void)selectByAry:(NSArray*)ary;

@end


@interface NoticeCell : UITableViewCell
{
    NSInteger selectNum;
}

@property(nonatomic,assign)id<deleteTixingDelegate>delegate;

@property(nonatomic,strong)IBOutlet UILabel * dateLabel;
@property(nonatomic,strong)IBOutlet UILabel * titleLabel;
@property(nonatomic,strong)IBOutlet UILabel * contentLabel;//
@property(nonatomic,strong)IBOutlet UIButton * deletebutton;//
@property(nonatomic,strong)NSMutableArray * dataAry;//

-(IBAction)deleteButtonClick:(id)sender;
-(void)displayDic:(NSDictionary *)mdic indexNum:(NSInteger)num;


@end
