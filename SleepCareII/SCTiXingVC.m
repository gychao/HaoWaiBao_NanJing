//
//  SCTiXingVC.m
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCTiXingVC.h"
#import "NoticeCell.h"
#import "SCNetManager.h"
#import "SCTiXingDetailVC.h"
@interface SCTiXingVC ()

@end

@implementation SCTiXingVC

@synthesize deleteAry;
@synthesize deleteButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataAry=[[NSMutableArray alloc]init];
    
    self.noticeTab.layer.borderWidth=3.0;
    self.noticeTab.layer.borderColor=[[UIColor colorWithRed:46.0/255 green:138.0/255 blue:208.0/255 alpha:1.0]CGColor];
    self.noticeTab.layer.cornerRadius=5.0;
    [self.noticeTab.layer setMasksToBounds:YES];
    
    self.noticeTab.separatorStyle =UITableViewCellSeparatorStyleNone;
}
-(void)viewWillAppear:(BOOL)animated
{
      self.navigationController.navigationBarHidden=YES;
   // self.view.frame=CGRectMake(0, 64+44, 1024, 768);
    
  //   self.view.bounds=CGRectMake(0, -64, 1024, 768);
    
    [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
        NSLog(@"response======%@",response);
        if ([response isKindOfClass:[NSDictionary class]]) {
            [dataAry removeAllObjects];
            NSArray * ary =[response objectForKey:@"remainList"];
            [dataAry addObjectsFromArray:ary];
            [self.noticeTab reloadData];
        }
    } faileture:^(BOOL faileTure) {
        
    } withType:kgetSleepRemind andWithPeroid:0];

}
-(IBAction)deleteButtonClick:(id)sender
{
    if (self.deleteAry.count>0) {
        
       // NSArray * ary =[NSArray arrayWithObject:[NSNumber numberWithInt:regID]];
        NSMutableArray * ary =[NSMutableArray arrayWithCapacity:0];
        for(NSDictionary * mdic in self.deleteAry){
        
            NSInteger regID =[[mdic objectForKey:@"regId"]integerValue];
            NSInteger isselect =[[mdic objectForKey:@"isSelect"]integerValue];
            if ( isselect==1) {
                [ary addObject:[NSNumber numberWithInt:regID]];
            }
        }
        
        [SCNetManager deleteSleepRemindSuccess:^(BOOL success, NSDictionary *response) {
            NSLog(@"%@",response);
            NSInteger  errorcode=[[response objectForKey:@"errorCode"]integerValue];
            if (errorcode==-1100) {
                UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"删除成功" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                [alert show];
                
                [self viewWillAppear:YES];
                
            }else{
                UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"参数有误,删除失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        } faileture:^(BOOL faileTure) {
            
        } withAry:ary andCount:ary.count];

        
    }else{
    
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"您没有选择要删除的提醒" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)selectByAry:(NSArray *)ary
{
    self.deleteAry=[NSMutableArray arrayWithArray:ary];

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataAry.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"ListCell";
    NoticeCell *cell = (NoticeCell*)(UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        UIViewController * cellcontroller =[[UIViewController alloc]initWithNibName:@"NoticeCell" bundle:nil];
        cell =(NoticeCell*)cellcontroller.view;
    }
   
    if (dataAry.count) {
        NSDictionary * mdic =[dataAry objectAtIndex:indexPath.row];
        cell.dataAry=dataAry;
        [cell displayDic:mdic indexNum:indexPath.row];
    }
    cell.delegate=self;
    [tableView  setSeparatorColor:[UIColor colorWithRed:52.0/255 green:142.0/255 blue:253.0/255 alpha:0.35]];  //设置分割线为蓝色
    cell.selectionStyle =UITableViewCellSelectionStyleGray;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * mdic  =[dataAry objectAtIndex:indexPath.row];
    SCTiXingDetailVC * detial =[[SCTiXingDetailVC alloc]initWithNibName:@"SCTiXingDetailVC_iPad" bundle:nil];
    [detial displayDic:mdic];
    [self.navigationController pushViewController:detial animated:YES];
    

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
