//
//  SCLast20Cell.m
//  SleepCareII
//
//  Created by dilitech on 14-6-16.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCLast20Cell.h"

#define QingSe [UIColor colorWithRed:0.8784 green:0.9490 blue:0.9765 alpha:1]

@implementation SCLast20Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    NSString *nibName =nil;
    if ([SCShareFunc isIPhone]) {
        nibName =@"_20MinCell_iPhone";
    }else{
        nibName =@"_20MinCell_iPad";
    }
    self =[[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil] lastObject];
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)displayViewUse:(NSDictionary *)dic andRow:(int)row{
    if (dic) {
        NSArray *labelArr =@[_breath,_time,_heartbeat,_apneaTime];
        for (UILabel *label in labelArr) {
            [self getText:label fromDataDic:dic];
        }
        if (row%2) {
            self.backgroundColor =[UIColor whiteColor];
        }else{
            self.backgroundColor =QingSe;
        }
    }
}

-(void)getText:(UILabel *)label fromDataDic:(NSDictionary *)dic{
    if (!label) {
        return;
    }
    NSString *key =[SCShareFunc nameWithInstance:label andInstanceOwner:self];
    NSString *value =[dic objectForKey:key] ;
    
    if (![SCShareFunc isNotEmptyStringOfObj:value]) {
        value =@"";
    }
    label.backgroundColor =[UIColor clearColor];
    label.text =value;
    label.hidden =NO;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
