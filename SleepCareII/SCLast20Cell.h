//
//  SCLast20Cell.h
//  SleepCareII
//
//  Created by dilitech on 14-6-16.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCLast20Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *heartbeat;
@property (weak, nonatomic) IBOutlet UILabel *breath;


@property (weak, nonatomic) IBOutlet UILabel *apneaTime;
-(void)displayViewUse:(NSDictionary *)dic andRow:(int)row;
@end
