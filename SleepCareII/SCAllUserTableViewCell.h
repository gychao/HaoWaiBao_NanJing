//
//  SCAllUserTableViewCell.h
//  SleepCareII
//
//  Created by dilitech on 14-6-12.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCAllUserTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *inBedImgView;
@property (weak, nonatomic) IBOutlet UILabel *heartBeatLabel;


@property (weak, nonatomic) IBOutlet UILabel *breathLabel;

@property (weak, nonatomic) IBOutlet UILabel *breathStopLabel;
@property (weak, nonatomic) IBOutlet UILabel *sleepQuaLabel;

@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *tapGes;

-(void)displayUseDic:(NSDictionary *)dic;
@end
