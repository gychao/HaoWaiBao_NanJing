//
//  SCShareFunc.m
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCShareFunc.h"

#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import <objc/runtime.h>


@implementation SCShareFunc


+(BOOL)isNotEmptyStringOfObj:(id)obj{
    BOOL isSring =NO;
    if (obj&&([obj isKindOfClass:[NSString class]])&&((NSString *)obj).length>0) {
        isSring =YES;
    }
    return isSring;
}

+(BOOL)systemVersonIsAfterNumber:(float)number{
    BOOL isAfter =NO;
    isAfter =(([[[UIDevice currentDevice] systemVersion] floatValue] >=number)?(YES):(NO));
    return isAfter;
}

+(BOOL)isIPhone{
    BOOL isPhone =NO;
    isPhone =(([[UIDevice currentDevice] userInterfaceIdiom] ==UIUserInterfaceIdiomPhone))?(YES):(NO);
    return isPhone;
}

+(void)displayHub{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
}
+(void)dismissHub{
    [SVProgressHUD dismiss];
}
+(NSString *)getServerUrlString{
    NSString *plistFilePath =[[NSBundle mainBundle] pathForResource:@"CompanyServer" ofType:@"plist"];
    NSString *appName =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    NSDictionary *dic =[NSDictionary dictionaryWithContentsOfFile:plistFilePath];
    NSString *serverUrl =[[dic objectForKey:appName] objectForKey:KServerUrl];
    return serverUrl;
}


+(NSDate *)dateFromString:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];// HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

+(NSDate *)dateFromTimeYearMonthDay:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy年MM月dd日"];// HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}


+(NSDate *)hour_dateFromString:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"HH:mm:ss"];// HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

+(NSString *)stringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];// HH:mm:ss zzz"];//

    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
    
}
+(NSString *)hour_stringFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    [dateFormatter setDateFormat:@"HH:mm:ss"];// HH:mm:ss zzz"];//
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
    
}


+(NSString *)dateStringToYearMonthDayFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];// HH:mm:ss zzz"];//
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}

+(NSString *)dateStringToMonthDayFromDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    [dateFormatter setDateFormat:@"MM月dd日"];// HH:mm:ss zzz"];//
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    return destDateString;
}

+(NSString *)nameWithInstance:(id)instance andInstanceOwner:(id)target
{
    unsigned int numIvars = 0;
    NSString *key=nil;
    Ivar * ivars = class_copyIvarList([target class], &numIvars);
    for(int i = 0; i < numIvars; i++) {
        Ivar thisIvar = ivars[i];
        const char *type = ivar_getTypeEncoding(thisIvar);
        NSString *stringType =  [NSString stringWithCString:type encoding:NSUTF8StringEncoding];
        if (![stringType hasPrefix:@"@"]) {
            continue;
        }
        if ((object_getIvar(target, thisIvar) == instance)) {
            key = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
            break;
        }
    }
    free(ivars);
    key =[[key componentsSeparatedByString:@"_"] lastObject];
    return key;
}

// 某月第一天
+(NSString *)firstDayStrFromMonth:(NSDate *)moth{
    NSString *To =nil;
    if (moth) {
        NSString * m =[SCShareFunc stringFromDate:moth];
        NSArray *mA =[m componentsSeparatedByString:@"-"];
        To =[NSString stringWithFormat:@"%@-%@-01",mA[0],mA[1]];
    }
    return To;
}

+(int)dayNumFormDateStr:(NSString *)str{
    int num =0;
    if ([SCShareFunc isNotEmptyStringOfObj:str]) {
        NSArray *arr =[str componentsSeparatedByString:@"-"];
        num =[[arr lastObject] intValue];
    }
    return num;
}


@end
