//
//  SCHistoryVC.h
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"
#import "TouchScrollView.h"
#import "ContentView.h"
@interface SCHistoryVC : SCBaseViewController<UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIButton * liftButton;// 生命体征
    IBOutlet UIButton * sleepButton;// 睡眠状态
    IBOutlet UIButton * sleepQuButton;// 睡眠质量
    IBOutlet UIView * bgView;// 整个大背景 蓝框的
    
    IBOutlet UIButton * oneWeekButton;//
    IBOutlet UIButton * twoWeekButton;//
    IBOutlet UIButton * oneMonth;// 一个月
    
    NSInteger days;// 7  14 28
    BOOL isLife;
    BOOL isSleep;
    BOOL isSleepQua;//是否是睡眠质量
    
    NSMutableArray * redPointAry;
    NSMutableArray * bluePointAry;
    NSMutableArray * greePointAry;
    
    IBOutlet TouchScrollView * pageScrollView;// 可以滑动的view
    IBOutlet ContentView * contentView;// 显示数据的View
    NSMutableArray * views;//
    
    NSInteger curPage;

    IBOutlet UIScrollView * bigScroll;//
    IBOutlet UIImageView * intruduceImage;
    
    NSMutableArray * TimeAry;
    IBOutlet UIView * autoView;//整体的view
    
    IBOutlet UILabel * bujialabel;//不佳
    IBOutlet UILabel * shangkelabel;// 尚可
    IBOutlet UILabel * youlianglabel;//优良
    
    BOOL isOneWeek;//
    BOOL isTwoWeek;
    BOOL isOneMonth;//
    
    
    NSMutableArray * ary1;
    NSMutableArray * ary2;
    NSMutableArray * ary3;//
    
    
    

}
@property(nonatomic,strong)IBOutlet UIImageView * intruduceImage;// 正方形 圆形 灵性图标

@property(nonatomic,strong)IBOutlet ContentView * contentView;
@property(nonatomic,strong)IBOutlet TouchScrollView * pageScrollView;
@property(nonatomic,strong)NSMutableArray * redPointAry;// 红点数组
@property(nonatomic,strong)NSMutableArray * bluePointAry;// 蓝点数组
@property(nonatomic,strong)NSMutableArray * greePointAry;// 绿点数组

@property(nonatomic,strong)NSMutableArray * TimeAry;// 时间间距数组


@property(nonatomic,strong)NSMutableArray *periodSleepQuiltyList;//
@property(nonatomic,strong)NSMutableArray *peroidInBedList;// 历史睡眠状态

@property(nonatomic,strong)NSMutableArray *MonthDayTimeArr;



@property (weak, nonatomic) IBOutlet UIButton *prePageBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextPageBtn;

-(IBAction)oneWeekButtonClick:(UIButton*)button;
-(IBAction)twoWeekButtonClick:(UIButton*)button;
-(IBAction)oneMonthButtonClick:(UIButton*)button;

-(IBAction)lifeButtonClick:(UIButton*)button;
-(IBAction)sleepButtonClick:(UIButton*)button;
-(IBAction)sleepQuationButtonClick:(UIButton*)button;
@end
