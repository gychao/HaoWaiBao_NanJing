//
//  ContentView.h
//  SleepCareII
//
//  Created by mengqinghao on 14-6-23.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentView : UIView<UITableViewDataSource,UITableViewDelegate>

{
    int x;
    int y;
    NSMutableArray * buttonAry;//
    NSInteger index;//
    
    UITableView * sleepTab;//
    
    NSMutableArray * SleepdateAry;
    NSMutableArray * riqiAry;// 睡眠状态日期
}

-(void)displayperoidInBedListBy:(NSArray*)inbedAry dateAry:(NSArray*)dateAry;// 睡眠状态
-(void)displayViewByType:(NSInteger)type redPointAry:(NSArray*)redpointAry greenPointAry:(NSArray*)greenPointAry bluePointAry:(NSArray*)bluePointAry dateAry:(NSArray*)dateAry;//生命体征
-(void)displayperoidSleepQuiltyList:(NSArray*)qulist dateAry:(NSArray*)dateAry;// 睡眠质量


@property(nonatomic,strong)NSMutableArray * redPointAry;//
@property(nonatomic,strong)NSMutableArray * bluePointAry;
@property(nonatomic,strong)NSMutableArray * greenPointAry;

@property(nonatomic,strong)NSMutableArray * SleepdateAry;//时间日期
@property(nonatomic,strong)NSMutableArray * riqiAry;// 日期

@property(nonatomic,strong)NSMutableArray * SleepQuityAry;//睡眠质量

@end
