//
//  SCDrawView.m
//  SleepCareII
//
//  Created by dilitech on 14-6-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCDrawView.h"

#define KTitleLabelCenter_Y 10

@implementation SCDrawView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithStyle:(enum drawStyle)style frame:(CGRect)frame{

    self =[super initWithFrame:frame];
    if (self) {
        _style =style;
        _titleLabel =[[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.textAlignment =NSTextAlignmentCenter;
        _titleLabel.textColor =[UIColor blackColor];
        _titleLabel.backgroundColor =[UIColor clearColor];
        [self addSubview:_titleLabel];
        
    }
    return self;
};

-(void)setStyle:(enum drawStyle)style{
    _style =style;
}

-(void)setTitle:(NSString *)title{
    if (!_titleLabel.superview) {
        [self addSubview:_titleLabel];
    }
    _titleLabel.center =(CGPoint){self.bounds.size.width/2,KTitleLabelCenter_Y};
    _titleLabel.text =title;
    _titleLabel.font =[UIFont systemFontOfSize:13];
    [_titleLabel sizeToFit];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
