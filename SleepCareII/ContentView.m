//
//  ContentView.m
//  SleepCareII
//
//  Created by mengqinghao on 14-6-23.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "ContentView.h"
#import "SleepStatusCell.h"
@implementation ContentView
@synthesize redPointAry;
@synthesize bluePointAry;
@synthesize greenPointAry;
@synthesize SleepdateAry;
@synthesize riqiAry;
@synthesize SleepQuityAry;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
  
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self setClearsContextBeforeDrawing: YES];

    
    if (index==1) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGFloat pointLineWidth = 1.5f;
        CGFloat pointMiterLimit = 5.0f;
        CGContextSetLineWidth(context, pointLineWidth);
        CGContextSetMiterLimit(context, pointMiterLimit);
//        CGContextSetLineJoin(context, kCGLineJoinMiter);
//        CGContextSetLineCap(context, kCGLineCapButt );
        CGContextSetBlendMode(context, kCGBlendModeNormal);
        UIColor* color1 = [UIColor redColor];
        [color1 set];
        //绘图
        CGPoint p1  = [[self.redPointAry objectAtIndex:0] CGPointValue];
        CGContextMoveToPoint(context, p1.x, p1.y);
        int i = 0;
        
        for(i=0;i<self.redPointAry.count;i++){
            p1 = [[redPointAry objectAtIndex:i] CGPointValue];
            CGContextAddLineToPoint(context, p1.x, p1.y);;
            UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
            [bt setBackgroundImage:[UIImage imageNamed:@"xintiaoshu"] forState:0];
            [bt setFrame:CGRectMake(0, 0, 10, 10)];
            [bt setCenter:p1];
            [self addSubview:bt];
        }
        CGContextStrokePath(context);
        
        UIColor* color = [UIColor blueColor];
        [color set];
        //绘图
        CGPoint p2  = [[self.bluePointAry objectAtIndex:0] CGPointValue];
        CGContextMoveToPoint(context, p2.x, p2.y);
        int j = 0;
        
        for(j=0;j<self.bluePointAry.count;j++){
            p2 = [[bluePointAry objectAtIndex:j] CGPointValue];
            CGContextAddLineToPoint(context, p2.x, p2.y);;
            UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
            [bt setBackgroundImage:[UIImage imageNamed:@"huxishu"] forState:0];
            [bt setFrame:CGRectMake(0, 0, 10, 10)];
            [bt setCenter:p2];
            [self addSubview:bt];
        }
        CGContextStrokePath(context);
        
        UIColor* color3 = [UIColor greenColor];
        [color3 set];
        //绘图
        CGPoint p3  = [[self.greenPointAry objectAtIndex:0] CGPointValue];
        CGContextMoveToPoint(context, p3.x, p3.y);
        int k = 0;
        for(k=0;k<self.greenPointAry.count;k++){
            p3 = [[greenPointAry objectAtIndex:k] CGPointValue];
            CGContextAddLineToPoint(context, p3.x, p3.y);;
            UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
            [bt setFrame:CGRectMake(0, 0, 10, 10)];
            [bt setBackgroundImage:[UIImage imageNamed:@"huxizantingshu"] forState:0];
            [bt setCenter:p3];
            [self addSubview:bt];
            
        }
        CGContextStrokePath(context);
    }
    
}

-(void)displayViewByType:(NSInteger)type redPointAry:(NSArray*)redpointAry greenPointAry:(NSArray*)greenpointAry bluePointAry:(NSArray*)bluepointAry dateAry:(NSArray*)dateAry;
{
    
    for(UIView * view in self.subviews){
        [view removeFromSuperview];
    }
    
    UILabel * introllabel =[[UILabel alloc]initWithFrame:CGRectMake((self.frame.size.width-80)/2, 10, 80, 30)];
    introllabel.backgroundColor=[UIColor clearColor];
    introllabel.text=@"单位:次数";
    introllabel.font=[UIFont systemFontOfSize:15];
    introllabel.textColor=[UIColor blackColor];
    [self addSubview:introllabel];
    
    if (type==1) {
        index=1;
        self.redPointAry=[[NSMutableArray alloc]init];
        self.bluePointAry=[[NSMutableArray alloc]init];
        self.greenPointAry=[[NSMutableArray alloc]init];
        [self.redPointAry removeAllObjects];
        [self.bluePointAry removeAllObjects];
        [self.greenPointAry removeAllObjects];
        NSArray * numAry =[NSArray arrayWithObjects:@"120",@"100",@"80",@"60",@"40",@"20",@"0", nil];
        for(int i=0;i<numAry.count;i++){
            UILabel * numlab =[[UILabel alloc]initWithFrame:CGRectMake(20, 45*(i+1), 30, 15)];
            numlab.text=[numAry objectAtIndex:i];
            numlab.textColor=[UIColor blackColor];
            numlab.textAlignment=NSTextAlignmentRight;
            [self addSubview:numlab];
            NSLog(@"numlab==========%@---%@",numlab,numlab.text);
            UIImageView * divline=[[UIImageView alloc]initWithFrame:CGRectMake(52, 46*(i+1), 10, 2)];
            divline.backgroundColor=[UIColor grayColor];
            [self addSubview:divline];
           
            UIImageView * bottmdivline=[[UIImageView alloc]initWithFrame:CGRectMake(82*(i+1), 325, 60, 25)];
            bottmdivline.backgroundColor=[UIColor clearColor];
            UIImageView * divimg=[[UIImageView alloc]initWithFrame:CGRectMake(29, 0, 2, 5)];
            divimg.backgroundColor=[UIColor grayColor];
            [bottmdivline addSubview:divimg];
            
            UILabel * bottomlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 5, 60, 20)];
            bottomlabel.font=[UIFont systemFontOfSize:13];
            bottomlabel.text=[dateAry objectAtIndex:i];
            [bottmdivline addSubview:bottomlabel];
            [self addSubview:bottmdivline];
            
            NSNumber * rednumber=[redpointAry objectAtIndex:i];
            NSInteger value=[rednumber intValue];
            CGFloat redoffy=((120-value)*280)/120.0;
            CGPoint point =CGPointMake(bottmdivline.center.x, redoffy+45);
            [self.redPointAry addObject:[NSValue valueWithCGPoint:point]];
            
            
            NSNumber * bluenumber=[bluepointAry objectAtIndex:i];
            NSInteger bvalue=[bluenumber intValue];
            CGFloat boffy=((120-bvalue)*280)/120.0;
            NSLog(@"offy=======%f",boffy);
            CGPoint bpoint =CGPointMake(bottmdivline.center.x, boffy+45);
            [self.bluePointAry addObject:[NSValue valueWithCGPoint:bpoint]];
            
            NSNumber * gnumber=[greenpointAry objectAtIndex:i];
            NSInteger gvalue=[gnumber intValue];
            CGFloat goffy=((120-gvalue)*280)/120.0;
            NSLog(@"offy=======%f",boffy);
            CGPoint gpoint =CGPointMake(bottmdivline.center.x, goffy+45);
            [self.greenPointAry addObject:[NSValue valueWithCGPoint:gpoint]];
            
        }
        UIImageView * leftline=[[UIImageView alloc]initWithFrame:CGRectMake(62, 45, 2, 280)];
        leftline.backgroundColor=[UIColor grayColor];
        [self addSubview:leftline];
        UIImageView * bottomline=[[UIImageView alloc]initWithFrame:CGRectMake(62, 325, 600, 2)];
        bottomline.backgroundColor=[UIColor grayColor];
        [self addSubview:bottomline];
        [self setNeedsDisplay];

        
    }


}

-(void)displayperoidInBedListBy:(NSArray*)inbedAry dateAry:(NSArray*)dateAry
{
    for(UIView * view in self.subviews){
        [view removeFromSuperview];
    }
    sleepTab=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 330) style:UITableViewStylePlain];
    sleepTab.separatorStyle =UITableViewCellSeparatorStyleNone;
    sleepTab.delegate=self;
    sleepTab.dataSource=self;
    [self addSubview:sleepTab];
    NSArray * ary =[NSArray arrayWithObjects:@"12:00",@"16:00",@"20:00",@"00:00",@"04:00",@"08:00",@"12:00", nil];
    CGFloat x_point_1 =  (self.bounds.size.width-(ary.count-1)*2)/ary.count;
    CGFloat x_point;
    for(int i=0;i<ary.count;i++){
        x_point =x_point_1 *(i+1)+2*i;
        
        UILabel * bottomlabel=[[UILabel alloc] initWithFrame:CGRectZero];
        bottomlabel.backgroundColor=[UIColor clearColor];
        bottomlabel.text=[ary objectAtIndex:i];
       // [bottomlabel setCenter:CGPointMake(120*i+96, 330)];
        bottomlabel.textAlignment=NSTextAlignmentCenter;
        bottomlabel.font=[UIFont systemFontOfSize:15];
        [bottomlabel sizeToFit];
        
        CGRect frame =(CGRect){x_point-bottomlabel.bounds.size.width,330,bottomlabel.bounds.size.width,bottomlabel.bounds.size.height};
        bottomlabel.frame =frame;
        
        [self addSubview:bottomlabel];
    
    }
    self.SleepdateAry=[[NSMutableArray alloc]initWithArray:inbedAry];
    self.riqiAry=[[NSMutableArray alloc]initWithArray:dateAry];
    [sleepTab reloadData];
    

}
-(void)displayperoidSleepQuiltyList:(NSArray*)qulist dateAry:(NSArray*)dateAry
{
    [self setNeedsDisplay];
    for(UIView * view in self.subviews){
        [view removeFromSuperview];
    }
    [self.redPointAry removeAllObjects];
    [self.greenPointAry removeAllObjects];
    [self.bluePointAry removeAllObjects];
    [self.SleepdateAry removeAllObjects];
    [self.riqiAry removeAllObjects];
    index=3;
    if (self.SleepQuityAry==nil) {
        self.SleepQuityAry=[[NSMutableArray alloc]init];
    }
    [self.SleepQuityAry removeAllObjects];

    [self.SleepQuityAry addObjectsFromArray:qulist];
    
    
    UILabel * introllabel =[[UILabel alloc]initWithFrame:CGRectMake((self.frame.size.width-80)/2, 10, 80, 30)];
    introllabel.backgroundColor=[UIColor clearColor];
    introllabel.text=@"单位:分数";
    introllabel.font=[UIFont systemFontOfSize:15];
    introllabel.textColor=[UIColor blackColor];
    [self addSubview:introllabel];
    
    
    
    
    NSArray * numAry =[NSArray arrayWithObjects:@"120",@"100",@"80",@"60",@"40",@"20",@"0", nil];
    for(int i=0;i<numAry.count;i++){
        UILabel * numlab =[[UILabel alloc]initWithFrame:CGRectMake(20, 45*(i+1), 30, 15)];
      
        numlab.textColor=[UIColor blackColor];
        numlab.textAlignment=NSTextAlignmentRight;
        [self addSubview:numlab];
        UIImageView * divline=[[UIImageView alloc]initWithFrame:CGRectMake(52, 46*(i+1), 10, 2)];
        if (i==0) {
            numlab.text=@"";
            divline.backgroundColor=[UIColor clearColor];
        }else{
            numlab.text=[numAry objectAtIndex:i];
            divline.backgroundColor=[UIColor grayColor];
        }
        [self addSubview:divline];
    }
    
    if (dateAry.count) {
        
        for(int i=0;i<dateAry.count;i++){
            
            UIImageView * bottmdivline=[[UIImageView alloc]initWithFrame:CGRectMake(75*(i+1), 325, 60, 25)];
            bottmdivline.backgroundColor=[UIColor clearColor];
            UIImageView * divimg=[[UIImageView alloc]initWithFrame:CGRectMake(29, 0, 2, 5)];
            divimg.backgroundColor=[UIColor grayColor];
            [bottmdivline addSubview:divimg];
            
            UILabel * bottomlabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 5, 60, 20)];
            bottomlabel.font=[UIFont systemFontOfSize:13];
            bottomlabel.text=[dateAry objectAtIndex:i];
            [bottmdivline addSubview:bottomlabel];
            [self addSubview:bottmdivline];
            
            NSNumber * rednumber=[self.SleepQuityAry objectAtIndex:i];
            NSInteger value=[rednumber intValue];
            CGFloat redoffy=((120-value)*280)/120;
            CGPoint point =CGPointMake(bottmdivline.center.x, redoffy+45);
            UIImageView * zhuziImagev=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 5)];
          //  zhuziImagev.center=CGPointMake(bottmdivline.center.x, redoffy+45+(100-redoffy)/2);
            CGRect frame;
            frame.origin.x=bottmdivline.center.x-10;
            frame.origin.y=redoffy+45;
            CGSize size=CGSizeMake(20, 235-redoffy+45);
            frame.size=size;
            zhuziImagev.frame=frame;
            [self addSubview:zhuziImagev];
            
            UILabel * numlabel =[[UILabel alloc]initWithFrame:CGRectMake(zhuziImagev.frame.origin.x, zhuziImagev.frame.origin.y-20, 20, 20)];
            numlabel.text=[NSString stringWithFormat:@"%d",value];
            numlabel.backgroundColor=[UIColor clearColor];
            if (value<=69) {
                zhuziImagev.backgroundColor=[UIColor redColor];
                numlabel.textColor=[UIColor redColor];
            }else if (value>=70&&value<=79){
                zhuziImagev.backgroundColor=[UIColor yellowColor];
                numlabel.textColor=[UIColor yellowColor];
            }else if (value>=80&&value<=100){
                zhuziImagev.backgroundColor=[UIColor greenColor];
                numlabel.textColor=[UIColor greenColor];
            }
            [self addSubview:numlabel];
            [self.SleepQuityAry addObject:[NSValue valueWithCGPoint:point]];
            
        }
    }
    UIImageView * leftline=[[UIImageView alloc]initWithFrame:CGRectMake(62, 45, 2, 280)];
    leftline.backgroundColor=[UIColor grayColor];
    [self addSubview:leftline];
    UIImageView * bottomline=[[UIImageView alloc]initWithFrame:CGRectMake(62, 325, 600-50, 2)];
    bottomline.backgroundColor=[UIColor grayColor];
    [self addSubview:bottomline];
    

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return SleepdateAry.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SleepStatusCell * cell=[tableView dequeueReusableCellWithIdentifier:@"ID"];
    if (cell==nil) {
        UIViewController * cellcontroller =[[UIViewController alloc]initWithNibName:@"SleepStatusCell" bundle:nil];
        cell =(SleepStatusCell*)cellcontroller.view;
    }
    NSLog(@"%d",indexPath.row);
    [cell displayTime:[SleepdateAry objectAtIndex:indexPath.row] dateTime:[riqiAry objectAtIndex:indexPath.row]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}



@end
