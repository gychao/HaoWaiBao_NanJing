//
//  SCRootViewController.h
//  SleepCareII
//
//  Created by dilitech on 14-6-11.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"
#import "SCAllUserTableView.h"
#import "SCAllUserTableViewCell.h"

@class SCAllUserTableView;

@interface SCRootViewController : SCBaseViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)UITabBarController *tabBarVC;


@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (weak, nonatomic) IBOutlet UIView *leftControlView;
@property (weak, nonatomic) IBOutlet UILabel *topUserNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *topDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *topCalanderBtn;

@property (weak, nonatomic) IBOutlet UIView *coreTableBgView;


@property (weak, nonatomic) IBOutlet SCAllUserTableView *userInfoTableView;

@property(nonatomic,strong)NSDictionary *tabViewDataDic;

@property (weak, nonatomic) IBOutlet UIButton *quiteBtn;

- (IBAction)leftControlViewBtnClick:(UIButton *)sender;


-(void)displayAllUserInfoWithInfoDic:(NSDictionary *)infoDic;

-(void)clickTwiceAtTheCell:(SCAllUserTableViewCell *)cell;


@end
