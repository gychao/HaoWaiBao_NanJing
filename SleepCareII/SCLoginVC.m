//
//  SCLoginVC.m
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCLoginVC.h"
#import "SCNetManager.h"
#import "SCLoginLogicManager.h"
@interface SCLoginVC (){

@private
    BOOL _keyBoardIsOn;
    CGRect _loginviewOrgframe;//loginview原始frame
}
@end

#define KboardToPassY 20 //

@implementation SCLoginVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
     
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyBoard:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disappearKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    
    self.loginView.center =self.view.center;
    [self.view addSubview:self.loginView];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
    BOOL autoLogin =[[userDefaults objectForKey:KAutoLogin] boolValue];
    if (autoLogin) {
        // 有自动登录
        NSString *name =[userDefaults objectForKey:KUserName];
        NSString *password =[userDefaults objectForKey:KPassWord];
        self.nameField.text =name;
        self.passwordField.text =password;
        self.autoLoginBtn.selected =YES;
    }else{
        // 没有自动登录
        NSLog(@"%s",__func__);
//        self.nameField.text =nil;
//        self.passwordField.text =nil;
        self.autoLoginBtn.selected =NO;
    }
    
    // 默认显示login
    [self.loginView displaySelfWithType:loginStyle];
}
-(void)viewDidAppear:(BOOL)animated{
    // 自动登录
    [super viewDidAppear:animated];
    NSUserDefaults *userDefaults =[NSUserDefaults standardUserDefaults];
    BOOL autoLogin =[[userDefaults objectForKey:KAutoLogin] boolValue];
    if (autoLogin) {
        [self loginviewClickBtn:self.loginBtn];
    }
}

-(void)showKeyBoard:(NSNotification *)notification{
    NSDictionary *userinfo =notification.userInfo;
    NSLog(@"%@",userinfo);
    if (_keyBoardIsOn) {
        return;
    }
    _loginviewOrgframe =self.loginView.frame;
    _keyBoardIsOn =YES;
    
    CGRect NeedToFrame;
    double duration =[[userinfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect endRect =[[userinfo objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    float Y  =[UIScreen mainScreen].bounds.size.width-[[UIApplication sharedApplication] statusBarFrame].size.width-endRect.size.height -KboardToPassY -self.passwordField.frame.origin.y-self.passwordField.frame.size.height;
    NeedToFrame =(CGRect){_loginviewOrgframe.origin.x,Y,_loginviewOrgframe.size.width,_loginviewOrgframe.size.height};
    [UIView animateWithDuration:duration animations:^{
        self.loginView.frame =NeedToFrame;
    } completion:nil];

}
-(void)disappearKeyboard:(NSNotification *)notification{
    NSDictionary *userinfo =notification.userInfo;
    NSLog(@"%@",userinfo);
    _keyBoardIsOn =NO;
    double duration =[[userinfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.loginView.frame =_loginviewOrgframe;
    } completion:nil];
}

#pragma mark - textfiled delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - logic
- (IBAction)loginviewClickBtn:(UIButton *)sender {
    
    if (sender == self.autoLoginBtn) {
        self.autoLoginBtn.selected =!self.autoLoginBtn.selected;
    }else if (sender ==self.forgetPasswordBtn){
        [self.loginView displaySelfWithType:findPasswordStyle];
        
    }else if (sender ==self.loginBtn){
        [self resignFirstResponseOfTextField];
        [SCNetManager loginWithName:self.nameField.text andPassword:self.passwordField.text];
    }else if(sender ==self.cancelBtn){
        self.passwordField.text =nil;
    }else if (sender ==self.getPWBtn){
        // 找回密码
        [SCNetManager getPassWord:^(NSString *password){
            self.getPWLabel.text =[NSString stringWithFormat:@"您的密码为 : %@",password];
            [self.loginView displaySelfWithType:getPasswordStyle];
            
        } loginName:self.nameField.text userName:self.passwordField.text];
        
    }else if (sender ==self.backBtn){
        self.passwordField.text =nil;
        [self.loginView displaySelfWithType:loginStyle];
    }else if (sender ==self.knowPWBtn){
        self.passwordField.text =nil;
        [self.loginView displaySelfWithType:loginStyle];
    }
    
    
}
-(void)resignFirstResponseOfTextField{
    [self.nameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}
@end
