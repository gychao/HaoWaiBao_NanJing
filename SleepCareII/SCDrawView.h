//
//  SCDrawView.h
//  SleepCareII
//
//  Created by dilitech on 14-6-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

enum  drawStyle{
    lineStyle =10,
    barStyle
};

@interface SCDrawView : UIView{
    
    enum drawStyle  _style;
    UILabel *_titleLabel;
}
-(id)initWithStyle:(enum drawStyle)style frame:(CGRect)frame;
-(void)setStyle:(enum drawStyle)style;
-(void)setTitle:(NSString *)title;

@property(nonatomic,strong)NSArray *vDesc;
@property(nonatomic,strong)NSArray *hDesc;
//-(void)setVertical

@end
