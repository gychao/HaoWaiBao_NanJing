//
//  SCLoginVC.h
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"
#import "SCLoginView.h"

@interface SCLoginVC : SCBaseViewController<UITextFieldDelegate>

//@property(nonatomic,assign)BOOL autoLogin;

#pragma mark - logview

@property (weak, nonatomic) IBOutlet SCLoginView *loginView;
@property (weak, nonatomic) IBOutlet UIButton *autoLoginBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;

@property (weak, nonatomic) IBOutlet UIButton *getPWBtn;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *getPWLabel;
@property (weak, nonatomic) IBOutlet UIButton *knowPWBtn;

- (IBAction)loginviewClickBtn:(UIButton *)sender;




//
#pragma mark - add for iPhone
@property (weak, nonatomic) IBOutlet UIImageView *loginBgImageView;


@end
